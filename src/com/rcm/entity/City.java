package com.rcm.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "cities")
public class City implements Comparable<City> {

    @Id
    @Column(name = "city_id")
    private int id;

    @Column(name = "title_ru")
    private String name;

    @Column(name = "country_id")
    private int countryId;

    @Column(name = "region_id")
    private Integer regionId;

    @Column(name = "region_ru")
    private String regionRu;

    public City() {

    }

    public City(int id, String name) {
        super();
        this.id = id;
        this.name = name;
    }

    public int compareTo(City city) {
        return getName().compareTo(city.getName());
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getCountryId() {
        return countryId;
    }

    public void setCountryId(int countryId) {
        this.countryId = countryId;
    }

    public Integer getRegionId() {
        return regionId;
    }

    public void setRegionId(Integer regionId) {
        this.regionId = regionId;
    }

    public String getRegionRu() {
        return regionRu;
    }

    public void setRegionRu(String regionRu) {
        this.regionRu = regionRu;
    }
}
