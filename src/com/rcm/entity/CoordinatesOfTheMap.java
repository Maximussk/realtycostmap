package com.rcm.entity;

public class CoordinatesOfTheMap {

	private Double firstLatitude;
	private Double firstLongitude;
	private Double secondLatitude;
	private Double secondLongitude;

	public CoordinatesOfTheMap() {

	}

	public CoordinatesOfTheMap(Double firstLatitude, Double firstLongitude,
			Double secondLatitude, Double secondLongitude) {
		super();
		this.firstLatitude = firstLatitude;
		this.firstLongitude = firstLongitude;
		this.secondLatitude = secondLatitude;
		this.secondLongitude = secondLongitude;
	}

	public Double getFirstLatitude() {
		return firstLatitude;
	}

	public void setFirstLatitude(Double firstLatitude) {
		this.firstLatitude = firstLatitude;
	}

	public Double getFirstLongitude() {
		return firstLongitude;
	}

	public void setFirstLongitude(Double firstLongitude) {
		this.firstLongitude = firstLongitude;
	}

	public Double getSecondLatitude() {
		return secondLatitude;
	}

	public void setSecondLatitude(Double secondLatitude) {
		this.secondLatitude = secondLatitude;
	}

	public Double getSecondLongitude() {
		return secondLongitude;
	}

	public void setSecondLongitude(Double secondLongitude) {
		this.secondLongitude = secondLongitude;
	}

}
