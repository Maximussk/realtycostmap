package com.rcm.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "addresses")
public class AdvancedAddress extends AbstractAddress {
    
        @Column(name = "squareCost")
	private int squareCost;

	public int getSquareCost() {
		return squareCost;
	}

	public void setSquareCost(int squareCost) {
		this.squareCost = squareCost;
	}

	@Override
	public String toString() {
		return "адрес: " + getAddress() + ", стоимость м2 = " + squareCost;
	}
}
