package com.rcm.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name = "squarecost")
public class SquareCost {
    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;

    @OneToOne
    @JoinColumn(name = "city_id")
    private CitiesCoordinates citiesCoordinates;

    @Column(name = "min")
    private int min;

    @Column(name = "middle")
    private int middle;

    @Column(name = "max")
    private int max;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public CitiesCoordinates getCitiesCoordinates() {
        return citiesCoordinates;
    }

    public void setCitiesCoordinates(CitiesCoordinates citiesCoordinates) {
        this.citiesCoordinates = citiesCoordinates;
    }

    public int getMin() {
        return min;
    }

    public void setMin(int min) {
        this.min = min;
    }

    public int getMiddle() {
        return middle;
    }

    public void setMiddle(int middle) {
        this.middle = middle;
    }

    public int getMax() {
        return max;
    }

    public void setMax(int max) {
        this.max = max;
    }

}
