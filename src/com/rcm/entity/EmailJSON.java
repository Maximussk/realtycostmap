package com.rcm.entity;

public class EmailJSON {

	private boolean isEmailValid;
	private boolean isEmailExists;
	
	public boolean isEmailValid() {
		return isEmailValid;
	}
	
	public void setEmailValid(boolean isEmailValid) {
		this.isEmailValid = isEmailValid;
	}

	public boolean isEmailExists() {
		return isEmailExists;
	}

	public void setEmailExists(boolean isEmailExists) {
		this.isEmailExists = isEmailExists;
	}


}
