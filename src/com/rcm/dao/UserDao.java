package com.rcm.dao;

import java.util.List;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import com.rcm.entity.City;
import com.rcm.entity.Country;
import com.rcm.entity.Region;
import com.rcm.entity.User;

@SuppressWarnings("unchecked")
@Repository
public class UserDao {

    @Autowired
    private SessionFactory sessionFactory;

    public void saveHash(String email, String password, String hash) {
        User user = (User) sessionFactory.getCurrentSession().createCriteria(User.class).add(Restrictions.eq("email", email))
                .add(Restrictions.eq("password", password)).uniqueResult();

        user.setHash(hash);

        sessionFactory.getCurrentSession().update(user);
        sessionFactory.getCurrentSession().flush();
    }

    public boolean isRegistered(String email) {
        return sessionFactory.getCurrentSession().createCriteria(User.class).add(Restrictions.eq("email", email)).uniqueResult() != null;
    }

    public boolean isRegistered(String email, String password) {
        return sessionFactory.getCurrentSession().createCriteria(User.class).add(Restrictions.eq("email", email))
                .add(Restrictions.eq("password", password)).uniqueResult() != null;
    }

    public boolean isAuthorized(String hash) {
        return sessionFactory.getCurrentSession().createCriteria(User.class).add(Restrictions.eq("hash", hash)).uniqueResult() != null;
    }

    public void signUp(User user) {
        sessionFactory.getCurrentSession().save(user);
        sessionFactory.getCurrentSession().flush();
    }

    public List<Country> getCountriesList() {
        List<Country> countriesList = sessionFactory.getCurrentSession().createCriteria(Country.class).addOrder(Order.asc("name")).list();
        return countriesList;
    }

    public List<Region> getRegionsList(int countryId) {
        List<Region> regionsList = sessionFactory.getCurrentSession().createCriteria(Region.class)
                .add(Restrictions.eq("countryId", countryId)).addOrder(Order.asc("name")).list();
        return regionsList;

    }

    public List<City> getCitiesListByRegionId(int regionId) {
        List<City> citiesList = sessionFactory.getCurrentSession().createQuery("from City where regionId = :regionId group by name")
                .setParameter("regionId", regionId).list();
        return citiesList;
    }
  
    public void deleteHash(String hash) {
        sessionFactory.getCurrentSession().createQuery("update User set hash = :emptyHash where hash = :hash").setParameter("hash", hash)
                .setParameter("emptyHash", null).executeUpdate();
    }

}