package com.rcm.dao;

import java.io.IOException;
import java.nio.file.DirectoryStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Repository;

import com.rcm.entity.AbstractAddress;
import com.rcm.entity.Address;
import com.rcm.entity.AdvancedAddress;
import com.rcm.entity.CitiesCoordinates;
import com.rcm.entity.City;
import com.rcm.entity.Coordinates;
import com.rcm.entity.CoordinatesOfTheMap;
import com.rcm.entity.SquareCost;
import com.rcm.utils.CityNameConverter;

@SuppressWarnings("unchecked")
@Repository
@PropertySource("classpath:statistics.properties")
public class RcmDao {

    @Autowired
    private SessionFactory sessionFactory;
    
    @Autowired
    private Environment env;

    public List<Address> getAddressesList(CoordinatesOfTheMap coordinatesOfTheMap) {
        double firstLongitude = coordinatesOfTheMap.getFirstLongitude();
        double secondLongitude = coordinatesOfTheMap.getSecondLongitude();
        double firstLatitude = coordinatesOfTheMap.getFirstLatitude();
        double secondLatitude = coordinatesOfTheMap.getSecondLatitude();
        List<Address> addressesList = (List<Address>) sessionFactory.getCurrentSession().createCriteria(Address.class)
                .add(Restrictions.between("longitude", firstLongitude, secondLongitude))
                .add(Restrictions.between("latitude", firstLatitude, secondLatitude)).list();

        return addressesList;
    }

    public List<AdvancedAddress> getAdvancedAddressesList(CoordinatesOfTheMap coordinatesOfTheMap) {
        double firstLongitude = coordinatesOfTheMap.getFirstLongitude();
        double secondLongitude = coordinatesOfTheMap.getSecondLongitude();
        double firstLatitude = coordinatesOfTheMap.getFirstLatitude();
        double secondLatitude = coordinatesOfTheMap.getSecondLatitude();
        List<AdvancedAddress> addressesList = (List<AdvancedAddress>) sessionFactory.getCurrentSession()
                .createCriteria(AbstractAddress.class).add(Restrictions.between("longitude", firstLongitude, secondLongitude))
                .add(Restrictions.between("latitude", firstLatitude, secondLatitude)).list();

        return addressesList;
    }

    public List<City> getCitiesList() {

        List<Object[]> namesIdList = sessionFactory
                .getCurrentSession()
                .createQuery(
                        "select cs.name, a.citiesCoordinates.id from CitiesCoordinates cs, Address a "
                                + "where cs.id=a.citiesCoordinates group by cs.name").list();
        List<City> citiesList = new ArrayList<City>();

        for (Object[] obj : namesIdList) {

            City city = new City();

            String name = (String) obj[0];
            city.setName(name);

            int id = (Integer) obj[1];
            city.setId(id);

            citiesList.add(city);
        }

        return citiesList;
    }

    public Coordinates getCoordinates(int cityId) {
        Coordinates coordinatesJSON = new Coordinates();
        CitiesCoordinates citiesCoordinates = (CitiesCoordinates) sessionFactory.getCurrentSession().get(CitiesCoordinates.class, cityId);

        double latitude = citiesCoordinates.getLatitude();
        coordinatesJSON.setLatitude(latitude);

        double longitude = citiesCoordinates.getLongitude();
        coordinatesJSON.setLongitude(longitude);

        return coordinatesJSON;
    }

    public List<Path> getStatistics(int cityId) throws IOException {
        String statistics = env.getProperty("statistics");
        Path dir = Paths.get(statistics);

        String cityNameRus = getCityName(cityId);
        String cityNameEng = CityNameConverter.rusToEngTranlit(cityNameRus);
        cityNameEng = cityNameEng.replace('-', '_');

        String globPattern = cityNameEng + "*.csv";

        List<Path> pathList = new ArrayList<>();
        try (DirectoryStream<Path> entries = Files.newDirectoryStream(dir, globPattern)) {
            for (Path entry : entries) {
                Path path = entry.getFileName();
                pathList.add(path);
            }
        }

        return pathList;
    }

    private String getCityName(int cityId) {
        CitiesCoordinates citiesCoordinates = (CitiesCoordinates) sessionFactory.getCurrentSession().get(CitiesCoordinates.class, cityId);
        String cityName = citiesCoordinates.getName();
        return cityName;
    }

    public SquareCost getSquarecost(int cityId) {
        SquareCost squarecostJSON = new SquareCost();
        List<SquareCost> squarecostList = sessionFactory.getCurrentSession().createCriteria(SquareCost.class).
                add(Restrictions.eq("citiesCoordinates.id", cityId)).list();
        
        SquareCost squarecost = squarecostList.get(0);
        
        int min = squarecost.getMin();
        squarecostJSON.setMin(min);
        
        int middle = squarecost.getMiddle();
        squarecostJSON.setMiddle(middle);
        
        int max = squarecost.getMax();
        squarecostJSON.setMax(max);
        
        return squarecostJSON;
    }

    
}