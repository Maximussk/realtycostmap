package com.rcm.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CookieValue;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.rcm.entity.City;
import com.rcm.services.RcmService;
import com.rcm.services.UserService;

@Controller
public class RcmController {
	
	@Autowired
	private UserService userService;
	@Autowired
	private RcmService rcmService;

	@RequestMapping(value = "/rcm.action", method = RequestMethod.GET)
	public ModelAndView getRcm(
			@CookieValue(value = "hash", required = false) String hash) {
		ModelAndView mav = new ModelAndView("rcm");
		List<City> citiesList = rcmService.getCitiesList();
		mav.addObject("citiesList", citiesList);
		boolean isAuthorized = userService.isAuthorized(hash);

		if (isAuthorized) {
			return new ModelAndView("redirect:rcmadvanced.action");
		}

		return mav;
	}
}
