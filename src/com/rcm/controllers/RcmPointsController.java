package com.rcm.controllers;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CookieValue;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.rcm.entity.Address;
import com.rcm.entity.AdvancedAddress;
import com.rcm.entity.CoordinatesOfTheMap;
import com.rcm.services.RcmService;
import com.rcm.services.UserService;

@Controller
public class RcmPointsController {

    @Autowired
    private RcmService rcmService;
    @Autowired
    private UserService userService;

    @RequestMapping(value = "/rcmpoints.action", method = RequestMethod.GET, headers = "Accept=application/json", produces = { "application/json; charset=UTF-8" })
    public @ResponseBody List<Address> getPoints(@RequestParam(value = "x1", required = true) double firstLatitude,
            @RequestParam(value = "y1", required = true) double firstLongitude,
            @RequestParam(value = "x2", required = true) double secondLatitude,
            @RequestParam(value = "y2", required = true) double secondLongitude) {

        CoordinatesOfTheMap coordinatesOfTheMap = new CoordinatesOfTheMap(firstLatitude, firstLongitude, secondLatitude, secondLongitude);

        List<Address> addressesList = rcmService.getAddressesList(coordinatesOfTheMap);

        return addressesList;
    }

    @RequestMapping(value = "/rcmpointsadvanced.action", method = RequestMethod.GET, headers = "Accept=application/json", produces = { "application/json; charset=UTF-8" })
    public @ResponseBody List<AdvancedAddress> getPointsAdvanced(@RequestParam(value = "x1", required = true) double firstLatitude,
            @RequestParam(value = "y1", required = true) double firstLongitude,
            @RequestParam(value = "x2", required = true) double secondLatitude,
            @RequestParam(value = "y2", required = true) double secondLongitude, @CookieValue(value = "hash", required = true) String hash) {

        boolean isAuthorized = userService.isAuthorized(hash);

        CoordinatesOfTheMap coordinatesOfTheMap = new CoordinatesOfTheMap(firstLatitude, firstLongitude, secondLatitude, secondLongitude);

        List<AdvancedAddress> addressesList = new ArrayList<>();
        if (isAuthorized) {
            addressesList = rcmService.getAdvancedAddressesList(coordinatesOfTheMap);
        }

        return addressesList;
    }
}
