package com.rcm.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.rcm.entity.Coordinates;
import com.rcm.services.RcmService;

@Controller
public class CoordinatesController {
    
    @Autowired
    private RcmService rcmService;
    
    @RequestMapping(value = "/coordinates.action", method = RequestMethod.GET, headers = "Accept=application/json", produces = { "application/json; charset=UTF-8" })
    public @ResponseBody
    Coordinates getCoordinates(
                    @RequestParam(value = "cityId", required = true) int cityId) {
            
            Coordinates coordinatesJSON = rcmService.getCoordinates(cityId);
            return coordinatesJSON;
    }
}
