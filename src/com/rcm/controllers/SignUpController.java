package com.rcm.controllers;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CookieValue;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.rcm.entity.City;
import com.rcm.entity.Country;
import com.rcm.entity.EmailJSON;
import com.rcm.entity.Region;
import com.rcm.entity.User;
import com.rcm.services.UserService;

@Controller
public class SignUpController {

	@Autowired
	private UserService userService;

	@RequestMapping(value = "/signup.action", method = RequestMethod.GET)
	public @ResponseBody ModelAndView getCountriesList(
			HttpServletRequest request, HttpServletResponse response,
			@CookieValue(value = "hash", required = false) String hash) {
		ModelAndView mav = new ModelAndView("signup");

		boolean isAuthorized = userService.isAuthorized(hash);

		if (isAuthorized) {
			return new ModelAndView("redirect:rcmadvanced.action");
		}

		List<Country> countriesList = userService.getCountriesList();
		mav.addObject("countriesList", countriesList);
		List<Region> regionsList = getRegionsList(1);
		mav.addObject("regionsList", regionsList);
		return mav;
	}

	@RequestMapping(value = "/signup.action", method = RequestMethod.POST)
	public @ResponseBody ModelAndView postSignUP(
			@RequestParam(value = "firstname", required = true) String firstName,
			@RequestParam(value = "surname", required = true) String surname,
			@RequestParam(value = "birthday", required = true) String birthday,
			@RequestParam(value = "country_id", required = true) int countryId,
			@RequestParam(value = "region_id", required = true) int regionId,
			@RequestParam(value = "city_id", required = true) int cityId,
			@RequestParam(value = "email", required = true) String email,
			@RequestParam(value = "password", required = true) String password,
			@RequestParam(value = "security_question", required = false) String securityQuestion,
			@RequestParam(value = "answer", required = false) String answer) {

		User user = new User();
		user.setFirstname(firstName);
		user.setSurname(surname);
		user.setBirthday(birthday);
		user.setCountryId(countryId);
		user.setRegionId(regionId);
		user.setCityId(cityId);
		user.setEmail(email);
		user.setPassword(password);
		user.setSecurityQuestion(securityQuestion);
		user.setAnswer(answer);

		userService.signUp(user);

		return new ModelAndView("redirect:signin.action");
	}

	@RequestMapping(value = "/citiesbyregionid.action", method = RequestMethod.GET, headers = "Accept=application/json", produces = { "application/json; charset=UTF-8" })
	public @ResponseBody List<City> getCitiesListByRegionId(
			@RequestParam(value = "regionId", required = true) int regionId) {
		return userService.getCitiesListByRegionId(regionId);
	}

	@RequestMapping(value = "/regions.action", method = RequestMethod.GET, headers = "Accept=application/json", produces = { "application/json; charset=UTF-8" })
	public @ResponseBody List<Region> getRegionsList(
			@RequestParam(value = "countryId", required = true) int countryId) {
		return userService.getRegionsList(countryId);
	}

	@RequestMapping(value = "/email.action", method = RequestMethod.GET, headers = "Accept=application/json", produces = { "application/json; charset=UTF-8" })
	public @ResponseBody EmailJSON isEmail(
			@RequestParam(value = "email", required = true) String email) {
		boolean isEmailExists = false;
		boolean isEmailValid = userService.isEmailValid(email);

		if (isEmailValid) {
			isEmailExists = userService.isEmailExists(email);
		}

		EmailJSON emailJSON = new EmailJSON();
		emailJSON.setEmailValid(isEmailValid);
		emailJSON.setEmailExists(isEmailExists);
		return emailJSON;
	}

}
