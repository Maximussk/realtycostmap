package com.rcm.controllers;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CookieValue;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.rcm.services.UserService;

@Controller
public class SignInController {

    @Autowired
    private UserService userService;

    @RequestMapping(value = "/signin.action", method = RequestMethod.GET)
    public ModelAndView getSignIn(HttpServletResponse response, @RequestParam(value = "signout", required = false) String signOut,
            @CookieValue(value = "hash", required = false) String hash) {
        ModelAndView mav = new ModelAndView("signin");

        boolean isAuthorized = userService.isAuthorized(hash);

        if (signOut != null) {
            Cookie cookie = userService.signOut(hash, isAuthorized);
            if (cookie != null) {
                response.addCookie(cookie);
            }
            return new ModelAndView("redirect:rcm.action");
        }

        if (isAuthorized) {
            return new ModelAndView("redirect:rcmadvanced.action");
        }

        return mav;
    }

    @RequestMapping(value = "/signin.action", method = RequestMethod.POST)
    public ModelAndView postSignIn(HttpServletResponse response, @RequestParam(value = "email", required = true) String email,
            @RequestParam(value = "password", required = true) String password) {

        ModelAndView mav = new ModelAndView("signin");
        Cookie cookie = userService.signIn(email, password);
        if (cookie != null) {
            response.addCookie(cookie);
            return new ModelAndView("redirect:rcmadvanced.action");
        }

        return mav;
    }

}
