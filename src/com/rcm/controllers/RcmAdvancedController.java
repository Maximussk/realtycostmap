package com.rcm.controllers;

import java.io.IOException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CookieValue;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.rcm.entity.City;
import com.rcm.entity.SquareCost;
import com.rcm.services.RcmService;
import com.rcm.services.UserService;

@Controller
public class RcmAdvancedController {

	@Autowired
	private RcmService rcmService;
	@Autowired
	private UserService userService;

	@RequestMapping(value = "/rcmadvanced.action", method = RequestMethod.GET)
	public ModelAndView getRcmAdvanced(
			@CookieValue(value = "hash", required = false) String hash) {
		ModelAndView mav = new ModelAndView("rcmadvanced");

		boolean isAuthorized = userService.isAuthorized(hash);

		if (!isAuthorized) {
			return new ModelAndView("redirect:rcm.action");
		}

		List<City> citiesList = rcmService.getCitiesList();
		mav.addObject("citiesList", citiesList);
		return mav;
	}

	@RequestMapping(value = "/statistics.action", method = RequestMethod.GET)
        public @ResponseBody
        String getStatistics(
                        @RequestParam(value = "cityId", required = true) int cityId) throws IOException {
                
                String statistics = rcmService.getStatistics(cityId);
                return statistics;
        }
	
	@RequestMapping(value = "/squarecost.action", method = RequestMethod.GET, headers = "Accept=application/json", produces = { "application/json; charset=UTF-8" })
        public @ResponseBody
        SquareCost getSquarecost(
                        @RequestParam(value = "cityId", required = true) int cityId) throws IOException {
                
                SquareCost squarecostJSON = rcmService.getSquarecost(cityId);
                return squarecostJSON;
        }
}
