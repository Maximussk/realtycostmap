package com.rcm.services;

import java.io.IOException;
import java.nio.file.Path;
import java.util.Collections;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.rcm.dao.RcmDao;
import com.rcm.entity.Address;
import com.rcm.entity.AdvancedAddress;
import com.rcm.entity.City;
import com.rcm.entity.Coordinates;
import com.rcm.entity.CoordinatesOfTheMap;
import com.rcm.entity.SquareCost;

@Service
public class RcmService {

    @Autowired
    private RcmDao rcmDao;

    @Transactional
    public List<Address> getAddressesList(CoordinatesOfTheMap coordinatesOfTheMap) {
        return rcmDao.getAddressesList(coordinatesOfTheMap);
    }

    @Transactional
    public List<AdvancedAddress> getAdvancedAddressesList(CoordinatesOfTheMap coordinatesOfTheMap) {
        return rcmDao.getAdvancedAddressesList(coordinatesOfTheMap);
    }

    @Transactional
    public List<City> getCitiesList() {
        List<City> citiesList = rcmDao.getCitiesList();
        return citiesList;
    }

    @Transactional
    public Coordinates getCoordinates(int cityId) {
        Coordinates coordinatesJSON = rcmDao.getCoordinates(cityId);
        return coordinatesJSON;
    }

    @Transactional
    public String getStatistics(int cityId) throws IOException {
        List<Path> pathList = rcmDao.getStatistics(cityId);
        Collections.sort(pathList);
        String statistics = "files/statistics/" + pathList.get(pathList.size() - 1).toString();
        return statistics;
    }

    @Transactional
    public SquareCost getSquarecost(int cityId) {
        SquareCost squarecostJSON = rcmDao.getSquarecost(cityId);
        return squarecostJSON;
    }

}
