package com.rcm.services;

import java.util.List;
import java.util.UUID;

import javax.servlet.http.Cookie;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.rcm.dao.UserDao;
import com.rcm.entity.City;
import com.rcm.entity.Country;
import com.rcm.entity.Region;
import com.rcm.entity.User;
import com.rcm.utils.EmailValidator;

@Service
public class UserService {

    @Autowired
    private UserDao userDao;

    @Transactional
    public Cookie signIn(String email, String password) {
        boolean isRegistered = userDao.isRegistered(email, password);

        if (isRegistered) {            
            String hash = UUID.randomUUID().toString();
            userDao.saveHash(email, password, hash);
            Cookie cookie = new Cookie("hash", hash);
            return cookie;
        }
        
        return null;
    }

    @Transactional
    public boolean isAuthorized(String hash) {
        if (hash != null) {
            boolean isAuthorized = userDao.isAuthorized(hash);
            return isAuthorized;
        }
        return false;
    }

    @Transactional
    public boolean isEmailExists(String email) {
        boolean isRegistered = userDao.isRegistered(email);
        return isRegistered;
    }

    @Transactional
    public void signUp(User user) {
        userDao.signUp(user);
    }
    
    @Transactional
    public List<Country> getCountriesList() {
        List<Country> countriesList = userDao.getCountriesList();
        Country ruCountry = new Country(1, "Россия");
        ruCountry.setNameEn("Russia");
        countriesList.remove(ruCountry);
        countriesList.add(0, ruCountry);
        return countriesList;
    }
    
    @Transactional
    public List<Region> getRegionsList(int countryId) {
        List<Region> regionsList = userDao.getRegionsList(countryId);
        return regionsList;
    }

    @Transactional
    public List<City> getCitiesListByRegionId(int regionId) {
        List<City> citiesList = userDao.getCitiesListByRegionId(regionId);
        return citiesList;
    }
    
    @Transactional
    public Cookie signOut(String hash, boolean isAuthorized) {
        if (isAuthorized) {
            deleteHash(hash);
            Cookie cookie = new Cookie("hash", hash);
            cookie.setMaxAge(0);
            return cookie;
        }
        return null;
    }

    @Transactional
    public void deleteHash(String hash) {
        userDao.deleteHash(hash);
    }
    
    @Transactional
    public boolean isEmailValid(String email) {
        EmailValidator emailValidator = new EmailValidator();
        boolean isEmailValid = emailValidator.validate(email);
        return isEmailValid;
    }    
}
