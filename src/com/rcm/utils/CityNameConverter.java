package com.rcm.utils;

public class CityNameConverter {

    private CityNameConverter() {
    }

    public static String rusToEngTranlit(String cityName) {
        char[] abcCyr = { 'а', 'б', 'в', 'г', 'д', 'е', 'ё', 'ж', 'з', 'и', 'й', 'к', 'л', 'м', 'н', 'о', 'п', 'р', 'с', 'т', 'у', 'ф',
                'х', 'ц', 'ч', 'ш', 'щ', 'ъ', 'ы', 'ь', 'э', 'ю', 'я', 'А', 'Б', 'В', 'Г', 'Д', 'Е', 'Ё', 'Ж', 'З', 'И', 'Й', 'К', 'Л',
                'М', 'Н', 'О', 'П', 'Р', 'С', 'Т', 'У', 'Ф', 'Х', 'Ц', 'Ч', 'Ш', 'Щ', 'Ъ', 'Ы', 'Ь', 'Э', 'Ю', 'Я', ' ' };
        String[] abcLat = { "a", "b", "v", "g", "d", "e", "e", "zh", "z", "i", "y", "k", "l", "m", "n", "o", "p", "r", "s", "t", "u", "f",
                "h", "ts", "ch", "sh", "sch", "", "y", "", "e", "yu", "ya", "a", "b", "v", "g", "d", "e", "e", "zh", "z", "i", "y", "k",
                "l", "m", "n", "o", "p", "r", "s", "t", "u", "f", "h", "ts", "ch", "sh", "sch", "", "y", "", "e", "yu", "ya", "_" };

        StringBuilder builder = new StringBuilder();

        for (int i = 0; i < cityName.length(); i++) {
            int count = 0;
            for (int j = 0; j < abcCyr.length; j++) {
                if ((int) cityName.charAt(i) == (int) abcCyr[j]) {
                    builder.append(abcLat[j]);
                    count = count + 1;
                    break;
                }
            }
            if (count == 0) {
                builder.append(cityName.charAt(i));
            } else {
                count = 0;
            }
        }
        return builder.toString();
    }

}
