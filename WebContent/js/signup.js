$(document).ready(function () {
				$("#countriesList").change(function() {					
					var countryId = $("#countriesList option:selected").attr("value");
					var url = 'regions.action?countryId=' + countryId;
					var html = "<option selected></option>\r\n";	
					$("#citiesList").empty().append(html);
					$.getJSON( url, function(regionsList) {						
			        	for (var i = 0; i < regionsList.length; i++) {
			        		html = html + "<option value=" + regionsList[i].id + ">" + regionsList[i].name + "</option>" + "\r\n";			        		
			        		}
			        	$("#region_select").text(" Выберите регион.");	
			        	$("#regionsList").empty().append(html);
					});
				});
				
				$("#regionsList").change(function() {
					$("#region_select").empty();
					var regionId = $("#regionsList option:selected").attr("value");
					var url = 'citiesbyregionid.action?regionId=' + regionId;
					var html = "<option selected></option>\r\n";
					$.getJSON( url, function(citiesList) {
						for (var i = 0; i < citiesList.length; i++) {
			        		html = html + "<option value=" + citiesList[i].id + ">" + citiesList[i].name + "</option>" + "\r\n";
			        		}
			        	$("#citiesList").empty().append(html);						
						$("#city_select").text(" Выберите населённый пункт.");
					});										
				});			
				
				$("#citiesList").change(function() {
					$("#city_select").empty();									
				});		
			
				$("#email").change(function() {
					var email = $("#email").val(); 				
					var url = 'email.action?email=' + email;					
					$.getJSON( url, function(email) {					
						if (!email.emailValid) {							
							$("#email_valid").empty().text(" Введите корректный email!");						
						} else if (email.emailExists){
							$("#email").val("");
							$("#email_valid").empty().text(" Пользователь с таким email уже зарегистрирован!");						
						} else {
							$("#email_valid").empty();
						}		        	
					});
				});		
				
				$("#password").keyup(function() {
					$("#confirm_valid").empty();
					var password = $("#password").val();
					var regex = "((?=.*\\d)(?=.*[a-z])(?=.*[A-Z]).{8,14})" +
		            "|((?=.*\\d)(?=.*[а-я])(?=.*[А-Я]).{8,14})";
					if ($("#confirm_password").val().length != 0) {					
						$("#password_valid").empty().attr("style", "color: orange").text(" Желаете изменить пароль? Пожалуйста!");
						$("#confirm_password").val("");						
					}	else if (password.match(regex)) {
						$("#password_valid").empty().attr("style", "color: green").text(" Отличный пароль!");						
					} else if (password.length > 10) {
						$("#password_valid").empty().attr("style", "color: olive").text(" Хороший пароль!");						
					} else if (password.length >= 1){
						$("#password_valid").empty().attr("style", "color: red").text(" Пароль слишком простой!");
						setTimeout(function () {
							$("#password_valid").empty();
							}, 2700);
					}					
				});
				
				$("#confirm_password").keyup(function() {
					$("#password_valid").empty();
					var password = $("#password").val();
					var confirm_password = $("#confirm_password").val();
					if (password.length == 0) {
						$("#confirm_valid").empty().attr("style", "color: red").text(" Введите пароль полем выше!");						
						setTimeout(function () {
							$("#password").focus();
							$("#confirm_password").val("");
							}, 1000);						
						return;
					} else if (password != confirm_password) {						
						$("#confirm_valid").empty().attr("style", "color: red").text(" Пароли не совпадают!");
					} else {
						$("#confirm_valid").empty();
					}
				});
			});		