			$(document).ready(function () {
				var myMap = null;
				var longitude = 73.368212;
				var latitude = 54.989342;				
				var x1, x2, y1, y2 = 0;				
				var startZoom = 11;
				var r = 60;
				var isStartMap = true;
				
				ymaps.ready(init);

				function init() {
				    var bounds;
				    var objA = {			
						 		center : [ latitude, longitude ],		 						
								zoom : 11,
								controls: ['smallMapDefaultSet']				
							};
					
					var objB = {
							minZoom: 9,
				            maxZoom: 16,
				            suppressMapOpenBlock: true
							};
					 	
					if (isStartMap) {
						
						isStartMap = false;
						ymaps.geolocation.get().then(function (res) {
					        var mapContainer = $('#map'),
					            bounds = res.geoObjects.get(0).properties.get('boundedBy');					        
					       
					        latitude = (bounds[1][0] + bounds[0][0])/2;
					        longitude = (bounds[0][1] + bounds[1][1])/2;
					        
					        objA.center[0] = latitude;
					        objA.center[1] = longitude;
					        
							createRealtyCostMap(objA, objB);
					    }, function (e) {
					        // Если местоположение невозможно получить, то просто создаем карту.
					        createMap({
					            center: [latitude, longitude],
					            zoom: 11
					        });
					    });
					} else {
						createRealtyCostMap(objA, objB);
					}
				}
				
				function createRealtyCostMap(objA, objB) {
					myMap = new ymaps.Map('map', objA, objB);
					myMap.controls.remove('searchControl');
					getBounds();					
					loadPoints(x1, x2, y1, y2);
					myMap.events.add('boundschange', function (e) {	
						r = 60;
						getBounds();	
						defineRadius();
						myMap.geoObjects.removeAll();					
						loadPoints(x1, x2, y1, y2);						
					});
				}
				
				function loadPoints(x1, x2, y1, y2) {					
					var url = 'rcmpoints.action?x1=' + x1 + '&y1=' + y1 + '&x2=' + x2 + '&y2=' + y2;					
					$.getJSON( url, function( pointsArray ) {
						if (pointsArray == 0) {
							$("#not_indexed").css("visibility", "visible");
						} else {
							$("#not_indexed").css("visibility", "hidden");
				        	for (var i = 0; i < pointsArray.length; i++) {
				        		var color = pointsArray[i].color;
				        		var latitude = pointsArray[i].latitude;
				        		var longitude = pointsArray[i].longitude;
				        		
				        		if (color == "#FFFFFF") {			        			
				        			secondColor = "#FF0000";
				        		} else {
				        			secondColor = color;
				        		}			        		
				        		
				        		var circle = new ymaps.Circle([[ latitude, longitude ], r ],
						        		{},
						        		{fillColor : color, strokeColor: secondColor, strokeOpacity: 1, strokeWidth: 2});			        		
				        		myMap.geoObjects.add(circle);			        	
				        	}
						}
					});
				}	        
				
				function getBounds() {
					bounds = myMap.getBounds();
					x1 = bounds[0][0];
					x2 = bounds[1][0];
					y1 = bounds[0][1];
					y2 = bounds[1][1];
				}
				
				function defineRadius() {
					var tempZoom = myMap.getZoom();
					var deltaZoom = tempZoom - startZoom;			
					
					r = r - deltaZoom * 10;		
				}
				
				$("#citiesList").change(function() {				
	 				var cityId = $("#citiesList option:selected").attr("value");	 				
	 				var url = 'coordinates.action?cityId=' + cityId;				
	 				$.getJSON( url, function(coordinates) {
	 					longitude = coordinates.longitude;
	 					latitude = coordinates.latitude;
	 					myMap.destroy();
	 					init();
	 				});	 				
				});
			});