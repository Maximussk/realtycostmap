<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8" />
		<meta http-equiv="X-UA-Compatible" content="IE=edge" />
    	<meta name="viewport" content="width=device-width, initial-scale=1" />
    			
		<title>Регистрация</title>
		<script src="//code.jquery.com/jquery-1.11.0.min.js"></script>
		<script src="js/signup.js"></script>
		 <!-- Bootstrap core CSS -->
	    <link href="http://getbootstrap.com/dist/css/bootstrap.min.css" rel="stylesheet">
	
	    <!-- Custom styles for this template -->
	    <link href="http://getbootstrap.com/examples/signin/signin.css" rel="stylesheet">
	
	    <!-- Just for debugging purposes. Don't actually copy these 2 lines! -->
	    <!--[if lt IE 9]><script src="../../assets/js/ie8-responsive-file-warning.js"></script><![endif]-->
	    <script src="http://getbootstrap.com/assets/js/ie-emulation-modes-warning.js"></script>
	
	    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
	    <!--[if lt IE 9]>
	      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
	      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	    <![endif]-->
	</head>
	<body>
		<div class="container">
			<h1>Регистрация</h1>
		    <div class="row">
		        <form role="form" method="post">
		            <div class="col-lg-6">
		                <div class="form-group">
		                    <label for="firstname">Имя</label>
		                    <div class="input-group">
		                        <input type="text" class="form-control" name="firstname" id="firstname" pattern="[А-Яа-яA-Za-zЁё\\-]{2,}" placeholder="Иван" autofocus="autofocus" required title="Иван или Ivan">
		                        <span class="input-group-addon"><span class="glyphicon glyphicon-asterisk"></span></span>
		                    </div>
		                </div>
		                <div class="form-group">
		                    <label for="surname">Фамилия</label>
		                    <div class="input-group">
		                        <input type="text" class="form-control" name="surname" id="surname" pattern="[А-Яа-яA-Za-zЁё\\-]{2,}" placeholder="Иванов" required title="Иванов или Ivanov">
		                        <span class="input-group-addon"><span class="glyphicon glyphicon-asterisk"></span></span>
		                    </div>
		                </div>
		                <div class="form-group">
		                    <label for="birthday">Год рождения</label>
		                    <div class="input-group">
		                        <input type="number" class="form-control" min="1914" max="2011" name="birthday" placeholder="1984" id="birthday" required>
		                        <span class="input-group-addon"><span class="glyphicon glyphicon-asterisk"></span></span>
		                    </div>
		                </div>
		                <div class="form-group">
		                    <label for="country">Страна:</label>
		                    <div class="input-group">
		                        <select name="country_id" id="countriesList" class="form-control" required>
									<c:forEach var="each" items="${countriesList}">				
										<option value="${each.getId()}">${each.getName()}</option>
									</c:forEach>
								</select>
		                        <span class="input-group-addon"><span class="glyphicon glyphicon-asterisk"></span></span>
		                    </div>
		                </div>
		                <div class="form-group">
			                <label for="region">Регион:</label><span style="color: red" id="region_select"></span>
			                <div class="input-group">
		                        <select name="region_id" id="regionsList" required class="form-control">		                        	
									<c:forEach var="each" items="${regionsList}">
										<option value="${each.getId()}">${each.getName()}</option>
									</c:forEach>
								</select>
		                        <span class="input-group-addon"><span class="glyphicon glyphicon-asterisk"></span></span>
			                </div>
		                </div>
		                <div class="form-group">
			                <label for="city">Населённый пункт:</label><span style="color: red" id="city_select"></span>
			                <div class="input-group">
		                        <select name="city_id" id="citiesList" class="form-control" required>
									<c:forEach var="each" items="${citiesList}">
										<option value="${each.getId()}">${each.getName()}</option>
									</c:forEach>
								</select>
		                        <span class="input-group-addon"><span class="glyphicon glyphicon-asterisk"></span></span>
			                </div>
		                </div>
		                <div class="form-group">
		                    <label for="email">Email</label><span style="color: red" id="email_valid"></span>
		                    <div class="input-group">
		                        <input type="email" class="form-control" id="email" name="email" required>
		                        <span class="input-group-addon"><span class="glyphicon glyphicon-asterisk"></span></span>		                        
		                    </div>
		                </div>
		                <div class="form-group">
		                    <label for="password">Пароль</label><span style="color: red" id="password_valid"></span>
		                    <div class="input-group">
		                        <input type="password" class="form-control" name="password" id="password" required>
		                        <span class="input-group-addon"><span class="glyphicon glyphicon-asterisk"></span></span>
		                    </div>
		                </div>
		                <div class="form-group">
		                    <label for="confirm_password">Подтверждение пароля</label><span style="color: red;" id="confirm_valid"></span>
		                    <div class="input-group">
		                        <input type="password" class="form-control" name="confirm_password" id="confirm_password" required>
		                        <span class="input-group-addon"><span class="glyphicon glyphicon-asterisk"></span></span>		                        
		                    </div>
		                </div>
		                <div class="form-group">
			                <label for="security_question">Секретный вопрос:</label>
			                <div class="input-group">
		                        <select name="security_question" title="Выберите секретный вопрос" class="form-control" required>
									<option selected="selected" value="0">Девичья фамилия матери</option>
									<option value="1">Любимый писатель</option>
									<option value="2">Любимое блюдо</option>
									<option value="3">Имя домашнего питомца</option>
									<option value="4">Любимый фильм</option>
								</select>
		                        <span class="input-group-addon"><span class="glyphicon glyphicon-asterisk"></span></span>
			                </div>
			            </div>
			            <div class="form-group">
		                    <label for="answer">Ответ:</label>
		                    <div class="input-group">
		                        <input type="text" class="form-control" name="answer" required>
		                        <span class="input-group-addon"><span class="glyphicon glyphicon-asterisk"></span></span>
		                    </div>
		                </div>		                
		                <div>
			                <strong><span class="glyphicon glyphicon-asterisk"></span>Поле, обязательное для заполнения</strong>
			                <input type="submit" name="submit" id="submit" value="Регистрация" class="btn btn-primary pull-right">
		                </div>
		            </div>
		        </form>		        
		    </div>
		</div>
		<!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
	    <script src="http://getbootstrap.com/assets/js/ie10-viewport-bug-workaround.js"></script>		
	</body>
</html>