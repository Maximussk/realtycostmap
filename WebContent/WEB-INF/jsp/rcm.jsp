<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
	<head>
		<title>Тепловая карта недвижимости</title>
		<meta charset="utf-8" />
		<meta name="keywords" content="Тепловая карта недвижимости, квартиры">
  		<meta name="description" content="Тепловая карта недвижимости: стоимость квадратного метра жилья, рассчитанная на примере квартир">
		<script src="https://api-maps.yandex.ru/2.1/?lang=ru_RU" type="text/javascript"></script>
		<script src="//code.jquery.com/jquery-1.11.0.min.js"></script>
		<!-- <script src="js/rcm.js" type="text/javascript"></script> -->
		<script src="js/rcm.js" type="text/javascript"></script>
		<script src="js/yandex_metrika.js" type="text/javascript"></script>
	    <link rel="stylesheet" href="css/rcm.css" type="text/css">
	    
	    	
	</head>
	<body>
		<noscript>
			<div id="ya_metrika">
	    		<img src="https://mc.yandex.ru/watch/38601835?ut=noindex" alt=""/>
	    	</div>
		</noscript> 
		<div id="map">
			<div>
				<span id="not_indexed">К сожалению ваш город не проиндексирован. Попробуйте выбрать город из выпадающего списка слева вверху.</span>
				<a href="signin.action" class="view" id="signin">Войти</a>
				<a href="signup.action" class="view" id="signup">Создать аккаунт</a>
				<div class="view" id="cities">
					<select name="city_id" id="citiesList" class="size" required>
						<option selected="selected">Выберите город</option>						
						<c:forEach var="each" items="${citiesList}">
							<option value="${each.getId()}">${each.getName()}</option>
						</c:forEach>			
					</select>
				</div>
				<div id="cloud-data">
			        <div id="data">
			          <ul>
			            <li>
			              <h4>Стоимость квадратного метра руб/м2</h4>
			            </li>
			            <li>
			              <ul id="titles">
			                <li class="cheap">
			                  Сверхдешево
			                </li>                
			                <li class="expensive">
			                  Сверхдорого
			                </li>
			              </ul>
			            </li>
			            <li>
			              <ul id="linear">
			                <li class="left">
			                    <div class="round black"></div>
			                </li>
			                <li class="center">
			                    <div class="gradient">
			                    	<h4 id="price">min middle max</h4>
			                    </div>
			                </li>
			                <li class="right">
			                    <div class="round red">
			                        <div id="inner" class="inner-round white"></div>
			                    </div>
			                </li>
			              </ul>
			            </li>
			          </ul>
			        </div>
      			</div>
			</div>
		</div>
	</body>
</html>