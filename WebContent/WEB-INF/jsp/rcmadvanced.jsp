<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
	<head>
		<title>Тепловая карта недвижимости</title>
		<meta charset="utf-8" />
		<meta name="keywords" content="Тепловая карта недвижимости, стоимость квадратного метра, квартиры">
  		<meta name="description" content="Тепловая карта недвижимости: стоимость квадратного метра жилья, рассчитанная на примере квартир; руб/м2">
		<script src="https://api-maps.yandex.ru/2.1/?lang=ru_RU" type="text/javascript"></script>
		<script src="//code.jquery.com/jquery-1.11.0.min.js"></script>
		<script src="js/rcmadvanced.js" type="text/javascript"></script>
		<script src="js/yandex_metrika.js" type="text/javascript"></script>
		<link rel="stylesheet" href="css/rcmadvanced.css" type="text/css">		
	</head>	
	<body>
		<div id="map">
			<noscript>
				<div id="ya_metrika">
		    		<img src="https://mc.yandex.ru/watch/38601835?ut=noindex" alt=""/>
		    	</div>
			</noscript>
			<div>
				<span id="not_indexed">К сожалению ваш город не проиндексирован. Попробуйте выбрать город из выпадающего списка слева вверху.</span>
				<a href="signin.action?signout" class="href">Выйти</a>
				<div class="view">
					<select name="city_id" id="citiesList" class="size" required>
						<option selected="selected">Выберите город</option>						
						<c:forEach var="each" items="${citiesList}">
							<option value="${each.getId()}">${each.getName()}</option>
						</c:forEach>			
					</select>
				</div>
					<a id="stat" href="." class="stat">Статистика</a>
				<div>
				</div>
				<div id="cloud-data">
			        <div id="data">
			          <ul>
			            <li>
			              <h4>Стоимость квадратного метра руб/м2</h4>
			            </li>
			            <li>
			              <ul id="titles">
			                <li class="cheap">
			                  Сверхдешево
			                </li>                
			                <li class="expensive">
			                  Сверхдорого
			                </li>
			              </ul>
			            </li>
			            <li>
			              <ul id="linear">
			                <li class="left">
			                    <div class="round black"></div>
			                </li>
			                <li class="center">
			                    <div class="gradient">
			                    	<h4 id="price">min middle max</h4>			                    			                    	
			                    </div>
			                </li>
			                <li class="right">
			                    <div class="round red">
			                        <div id="inner" class="inner-round white"></div>
			                    </div>
			                </li>
			              </ul>
			            </li>
			          </ul>
			        </div>
      			</div>
			</div>
		</div>		   		
	</body>
</html>